from django.urls import path, include

from rest_framework.routers import DefaultRouter

from employees import views


urlpatterns = [
    path(
        'employees/',
        views.EmployeeListViewSet.as_view(),
        name='employee-list'
    ),
    path(
        'employees/<int:pk>',
         views.EmployeeDetailViewSet.as_view(),
         name='employee-detail'
     )
]
