import factory
import random

from employees import models


Employee = models.Employee
department_choices = [department[0] for department in Employee.DEPARTMENT_CHOICES]
get_random_department = lambda: random.choice(department_choices)

class EmployeeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Employee

    name = factory.Faker('name')
    email = factory.Faker('email')
    department = factory.LazyFunction(get_random_department)
