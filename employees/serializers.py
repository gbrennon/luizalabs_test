from employees import models
from rest_framework import serializers

Employee = models.Employee


class EmployeeSerializer(serializers.ModelSerializer):
    department = serializers.CharField(source='get_department_display')

    class Meta:
        model = Employee
        fields = '__all__'

    def create(self, validated_data):
        validated_data['department'] = validated_data.pop(
            'get_department_display',
            None
        )
        return Employee.objects.create(**validated_data)

    def validate_department(self, data):
        choices = [c[0] for c in Employee.DEPARTMENT_CHOICES]

        if data not in choices:
            raise serializers.ValidationError(
                '{} is not a valid choice. Please select one of this: {}'.format(
                    data,
                    choices
                )
            )

        return data
