from rest_framework import generics

from employees import (
    models,
    serializers
)

# Create your views here.
class EmployeeListViewSet(generics.ListCreateAPIView):
    queryset = models.Employee.objects.all()
    serializer_class = serializers.EmployeeSerializer


class EmployeeDetailViewSet(generics.RetrieveDestroyAPIView):
    queryset = models.Employee.objects.all()
    serializer_class = serializers.EmployeeSerializer
