# Generated by Django 2.2.4 on 2019-08-15 20:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employees', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='department',
            field=models.CharField(choices=[('Architecture', 'Architecture'), ('E-commerce', 'E-commerce'), ('Mobile', 'Mobile'), ('Human Resources', 'Human Resources'), ('Infrastructure', 'Infrastructure'), ('Web Development', 'Web Development')], default='Architecture', max_length=50),
        ),
    ]
