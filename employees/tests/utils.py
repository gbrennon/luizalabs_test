from factory import Faker


def generate_field_value(field):
    return Faker(field).generate()
