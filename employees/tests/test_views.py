from django.test import TestCase, Client
from django.urls import reverse

from rest_framework import status

from employees import factories, serializers, models

from employees.tests.utils import generate_field_value

Employee = models.Employee
EmployeeFactory = factories.EmployeeFactory
EmployeeSerializer = serializers.EmployeeSerializer

# Create your tests here.
class EmployeeListViewTests(TestCase):
    def setUp(self):
        self.employees = [EmployeeFactory() for _ in range(10)]
        self.client = Client()

    def tearDown(self):
        [e.delete() for e in self.employees]

    def test_list_view_should_render_10_employees(self):
        response = self.client.get(reverse('employee-list'))

        serialized =  EmployeeSerializer(
            self.employees,
            many=True
        )

        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_a_valid_employee(self):
        e = EmployeeFactory.build()
        data = {
            'name': e.name,
            'email': e.email,
            'department': e.department
        }

        response = self.client.post(
            reverse('employee-list'),
            data
        )

        created_employee = Employee.objects.last()
        serialized = EmployeeSerializer(created_employee)

        self.assertDictEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_try_to_create_invalid_employee(self):
        data = {}

        response = self.client.post(
            reverse('employee-list'),
            data
        )

        expected_output = {
            'department': ['This field is required.'],
            'name': ['This field is required'],
            'email': ['This field is required'],
        }

        #couldn't get to compare the outputs
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class EmployeeDetailViewTests(TestCase):
    def setUp(self):
        self.employee = EmployeeFactory()
        self.client = Client()

    def tearDown(self):
        self.employee.delete()

    def test_get_a_created_employee(self):
        response = self.client.get(
            reverse(
                'employee-detail',
                kwargs={'pk': self.employee.pk}
            )
        )

        serialized = EmployeeSerializer(self.employee)

        self.assertDictEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_try_to_get_a_inexistent_employee(self):
        response = self.client.get(
            reverse(
                'employee-detail',
                kwargs={'pk': 200}
            )
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_an_existent_employee(self):
        response = self.client.delete(
            reverse(
                'employee-detail',
                kwargs={'pk': self.employee.pk}
            )
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_try_to_delete_an_inexistent_employee(self):
        response = self.client.delete(
            reverse(
                'employee-detail',
                kwargs={'pk': 200}
            )
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
