from django.test import TestCase
from django.core.exceptions import ValidationError

from employees import (
    factories,
    models,
    serializers
)

from employees.tests.utils import generate_field_value

import random

EmployeeFactory = factories.EmployeeFactory
Employee = models.Employee
EmployeeSerializer = serializers.EmployeeSerializer

# Create your tests here.
class EmployeeSerializerUnitTests(TestCase):
    def setUp(self):
        self.valid_department_choices = [
            c[0] for c in Employee.DEPARTMENT_CHOICES
        ]

        self.serializer_data = {
            'name': generate_field_value('name'),
            'email': generate_field_value('email'),
            'department': random.choice(
                self.valid_department_choices
            )
        }

    def test_valid_employee_data(self):
        serialized = EmployeeSerializer(
            data=self.serializer_data,
        )
        self.assertTrue(serialized.is_valid())

    def test_invalid_department_option(self):
        self.serializer_data['department'] = 'AK'

        serialized = EmployeeSerializer(
            data=self.serializer_data,
        )

        self.assertFalse(serialized.is_valid())
        self.assertNotIn(
            self.serializer_data['department'],
            self.valid_department_choices
        )
        self.assertEqual(
            set(serialized.errors),
            set(['department'])
        )
