from django.test import TestCase
from django.core.exceptions import ValidationError

from employees import factories
from employees import models

from employees.tests.utils import generate_field_value

EmployeeFactory = factories.EmployeeFactory
Employee = models.Employee

# Create your tests here.
class EmployeeUnitTests(TestCase):
    def setUp(self):
        self.employee = EmployeeFactory()

    def tearDown(self):
        self.employee.delete()

    def test_employee_str_method(self):
        expected_str = 'Employee {} - {}'.format(
            self.employee.pk,
            self.employee.name
        )

        self.assertEquals(expected_str, str(self.employee))


    def test_employee_with_empty_email_should_raise_exception(self):
        employee = Employee(
            email=generate_field_value('email')
        )

        with self.assertRaises(ValidationError):
            employee.save()


    def test_employee_with_invalid_email_should_raise_exception(self):
        employee = Employee(
            email=generate_field_value('name')
        )

        with self.assertRaises(ValidationError):
            employee.save()


    def test_employee_with_empty_name_should_raise_exception(self):
        employee = Employee(
            name=generate_field_value('name')
        )

        with self.assertRaises(ValidationError):
            employee.save()
