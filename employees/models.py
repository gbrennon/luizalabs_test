from django.db import models

# Create your models here.
class Employee(models.Model):
    DEPARTMENT_CHOICES = (
        ('Architecture', 'Architecture'),
        ('E-commerce', 'E-commerce'),
        ('Mobile', 'Mobile'),
        ('Human Resources', 'Human Resources'),
        ('Infrastructure', 'Infrastructure'),
        ('Web Development', 'Web Development'),
    )
    name = models.CharField(max_length=254, blank=False)
    email = models.EmailField(max_length=254, blank=False)
    department = models.CharField(
        max_length=50,
        choices=DEPARTMENT_CHOICES,
        default=DEPARTMENT_CHOICES[0][0],
    )

    def __str__(self):
        return 'Employee {} - {}'.format(
            self.pk,
            self.name
        )

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Employee, self).save(*args, **kwargs)
