#!/bin/bash
docker-compose build
docker-compose up -d
docker-compose run web python manage.py migrate
docker-compose run web python manage.py loaddata data.json
docker-compose run web python manage.py test

echo ''
echo 'Basic data created'
echo 'The admin user is created'
echo 'User: admin'
echo 'Password: 1q2w3e4r'
echo 'Check http://localhost:8000'
