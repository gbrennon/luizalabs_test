# Luizalabs Employees Challenge

## Description
- This is my code for the Luizalabs challenge.
- This is a Django based application and it uses Django Rest Framework.
- This is a REST API, but it doesn't contains the UPDATE(PUT) method for the employees because in the test description it was said that the API would LIST, CREATE, GET and DELETE.

## Running
- To run this application you need to install docker and docker-compose.
- You can start the app by typing `./bootstrap.sh` in your console.
- The application will be running under `localhost:8000`.
- The current valid routes for the api are `/admin` and `/api/employees`
- The valid departments for creating new employees are:
  - Architecture
  - E-commerce
  - Mobile
  - Human Resource
  - Infrastructure
  - Web Development
- The admin user/password is:
  - User: admin
  - Password: 1q2w3e4r

Enjoy!
